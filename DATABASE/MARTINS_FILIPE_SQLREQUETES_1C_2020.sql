SELECT * FROM `t_adresses` WHERE `RueAdresse` LIKE '29505 Batz Groves'

SELECT * FROM `t_adresses` WHERE `RueAdresse` LIKE '%29505%'

SELECT * FROM `t_adresses` WHERE `RueAdresse` NOT LIKE '29505'

SELECT * FROM `t_adresses` WHERE `RueAdresse` = '29505 Batz Groves'

SELECT * FROM `t_adresses` WHERE `RueAdresse` != '29505'

SELECT * FROM `t_adresses` WHERE `RueAdresse` REGEXP '[x-z]'

SELECT * FROM `t_adresses` WHERE `RueAdresse` NOT REGEXP '29505'

SELECT * FROM `t_adresses` WHERE `RueAdresse` REGEXP '^................$'

SELECT * FROM `t_adresses` WHERE `RueAdresse` NOT REGEXP '[a]'

SELECT * FROM `t_adresses` WHERE `NpaAdresse` IN (82104)

SELECT * FROM `t_adresses` WHERE `NpaAdresse` NOT IN (82104,322282)

SELECT * FROM `t_adresses` WHERE `NpaAdresse` BETWEEN 20000 AND 40000

SELECT * FROM `t_adresses` WHERE `NpaAdresse` NOT BETWEEN 20000 AND 40000

SELECT * FROM `t_adresses` WHERE `NpaAdresse` IS NOT NULL

SELECT * FROM `t_adresses` WHERE `NpaAdresse` IS NULL







/*
	Toutes les colonnes
*/
SELECT * FROM t_pers_adresse AS T1
INNER JOIN t_adresses AS T2 ON T2.id_adresse = T1.fk_adresse
INNER JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_pers

/*
	Seulement certaines colonnes
*/
SELECT id_personne,  , id_adresse, Date FROM t_pers_adresse AS T1
INNER JOIN t_adresses AS T2 ON T2.id_adresse = T1.fk_adresse
INNER JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_pers

/*
	Permet d'aficher toutes les lignes de la table de droite (t_personnes) (qui est écrite en sql à droite de t_pers_adresse)
	y compris les lignes qui ne sont pas attribuées à des films.
*/
SELECT id_personne, id_adresse, Date FROM t_pers_adresse AS T1
INNER JOIN t_adresses AS T2 ON T2.id_adresse = T1.fk_adresse
RIGHT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_pers

/*
	Permet d'aficher toutes les lignes de la table de droite (t_personnes) (qui est écrite en sql à droite de t_pers_adresse)
	y compris les lignes qui ne sont pas attribuées à des films.
*/
SELECT id_personne,  , id_adresse, Date  FROM t_pers_adresse AS T1
RIGHT JOIN t_adresses AS T2 ON T2.id_adresse = T1.fk_adresse
LEFT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_pers


/*
	Affiche TOUS les films qui n'ont pas de genre attribués
*/
SELECT id_personne,  , id_adresse, Date  FROM t_pers_adresse AS T1
RIGHT JOIN t_adresses AS T2 ON T2.id_adresse = T1.fk_adresse
LEFT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_pers


/*
	Affiche SEULEMENT les films qui n'ont pas de genre attribués
*/

SELECT id_personne,  , id_adresse, Date  FROM t_pers_adresse AS T1
RIGHT JOIN t_adresses AS T2 ON T2.id_adresse = T1.fk_adresse
LEFT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_pers
WHERE T1.fk_pers IS NULL
