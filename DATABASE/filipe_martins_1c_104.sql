--

-- Database: filipe_martins_1c_104
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists filipe_martins_1c_104;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS filipe_martins_1c_104;

-- Utilisation de cette base de donnée

USE filipe_martins_1c_104;



-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 10 Mars 2020 à 17:10
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `filipe_martins_1c_104`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_adresses`
--

CREATE TABLE `t_adresses` (
  `id_Adresse` int(11) NOT NULL,
  `RueAdresse` varchar(80) NOT NULL,
  `NpaAdresse` int(10) NOT NULL,
  `VilleAdresse` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_entreprise`
--

CREATE TABLE `t_entreprise` (
  `id_Entreprise` int(11) NOT NULL,
  `NomEntreprise` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_mails`
--

CREATE TABLE `t_mails` (
  `id_Mail` int(11) NOT NULL,
  `NomMail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_Personne` int(11) NOT NULL,
  `NomPers` varchar(50) NOT NULL,
  `PrenomPers` varchar(30) NOT NULL,
  `DateNaissPers` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_adresse`
--

CREATE TABLE `t_pers_adresse` (
  `id_pers_adresse` int(11) NOT NULL,
  `fk_pers` int(11) NOT NULL,
  `fk_adresse` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_entreprise`
--

CREATE TABLE `t_pers_entreprise` (
  `id_pers_entreprise` int(11) NOT NULL,
  `FonctionPersEntreprise` varchar(20) NOT NULL,
  `DateDebutContrat` date NOT NULL,
  `fk_pers` int(11) NOT NULL,
  `fk_entreprise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_mail`
--

CREATE TABLE `t_pers_mail` (
  `id_pers_mail` int(11) NOT NULL,
  `fk_pers` int(11) NOT NULL,
  `fk_mail` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_vehi`
--

CREATE TABLE `t_pers_vehi` (
  `id_pers_vehi` int(11) NOT NULL,
  `fk_pers` int(11) NOT NULL,
  `fk_vehicule` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_type_vehicule`
--

CREATE TABLE `t_type_vehicule` (
  `id_type_vehicule` int(11) NOT NULL,
  `MarqueVehicule` varchar(20) NOT NULL,
  `ModeleVehicule` varchar(20) NOT NULL,
  `Annee` date NOT NULL,
  `CarburantVehicule` varchar(10) NOT NULL,
  `fk_vehicule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_vehicules`
--

CREATE TABLE `t_vehicules` (
  `id_vehicule` int(11) NOT NULL,
  `immatriculationVehicule` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_vehi_entreprise`
--

CREATE TABLE `t_vehi_entreprise` (
  `id_vehi_entreprise` int(11) NOT NULL,
  `fk_vehicule` int(11) NOT NULL,
  `fk_entreprise` int(11) NOT NULL,
  `DateAchat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresses`
--
ALTER TABLE `t_adresses`
  ADD PRIMARY KEY (`id_Adresse`);

--
-- Index pour la table `t_entreprise`
--
ALTER TABLE `t_entreprise`
  ADD PRIMARY KEY (`id_Entreprise`);

--
-- Index pour la table `t_mails`
--
ALTER TABLE `t_mails`
  ADD PRIMARY KEY (`id_Mail`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_Personne`);

--
-- Index pour la table `t_pers_adresse`
--
ALTER TABLE `t_pers_adresse`
  ADD PRIMARY KEY (`id_pers_adresse`),
  ADD KEY `fk_pers` (`fk_pers`),
  ADD KEY `fk_adresse` (`fk_adresse`);

--
-- Index pour la table `t_pers_entreprise`
--
ALTER TABLE `t_pers_entreprise`
  ADD PRIMARY KEY (`id_pers_entreprise`),
  ADD KEY `fk_pers` (`fk_pers`),
  ADD KEY `fk_entreprise` (`fk_entreprise`);

--
-- Index pour la table `t_pers_mail`
--
ALTER TABLE `t_pers_mail`
  ADD PRIMARY KEY (`id_pers_mail`),
  ADD KEY `fk_pers` (`fk_pers`),
  ADD KEY `fk_mail` (`fk_mail`);

--
-- Index pour la table `t_pers_vehi`
--
ALTER TABLE `t_pers_vehi`
  ADD PRIMARY KEY (`id_pers_vehi`),
  ADD KEY `fk_pers` (`fk_pers`),
  ADD KEY `fk_vehicule` (`fk_vehicule`);

--
-- Index pour la table `t_type_vehicule`
--
ALTER TABLE `t_type_vehicule`
  ADD PRIMARY KEY (`id_type_vehicule`),
  ADD KEY `fk_vehicule` (`fk_vehicule`);

--
-- Index pour la table `t_vehicules`
--
ALTER TABLE `t_vehicules`
  ADD PRIMARY KEY (`id_vehicule`);

--
-- Index pour la table `t_vehi_entreprise`
--
ALTER TABLE `t_vehi_entreprise`
  ADD PRIMARY KEY (`id_vehi_entreprise`),
  ADD KEY `fk_vehicule` (`fk_vehicule`),
  ADD KEY `fk_entreprise` (`fk_entreprise`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_adresses`
--
ALTER TABLE `t_adresses`
  MODIFY `id_Adresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_entreprise`
--
ALTER TABLE `t_entreprise`
  MODIFY `id_Entreprise` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_mails`
--
ALTER TABLE `t_mails`
  MODIFY `id_Mail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_Personne` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_adresse`
--
ALTER TABLE `t_pers_adresse`
  MODIFY `id_pers_adresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_entreprise`
--
ALTER TABLE `t_pers_entreprise`
  MODIFY `id_pers_entreprise` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_mail`
--
ALTER TABLE `t_pers_mail`
  MODIFY `id_pers_mail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_vehi`
--
ALTER TABLE `t_pers_vehi`
  MODIFY `id_pers_vehi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_type_vehicule`
--
ALTER TABLE `t_type_vehicule`
  MODIFY `id_type_vehicule` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_vehicules`
--
ALTER TABLE `t_vehicules`
  MODIFY `id_vehicule` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_vehi_entreprise`
--
ALTER TABLE `t_vehi_entreprise`
  MODIFY `id_vehi_entreprise` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_pers_adresse`
--
ALTER TABLE `t_pers_adresse`
  ADD CONSTRAINT `t_pers_adresse_ibfk_1` FOREIGN KEY (`fk_pers`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_adresse_ibfk_2` FOREIGN KEY (`fk_adresse`) REFERENCES `t_adresses` (`id_Adresse`);

--
-- Contraintes pour la table `t_pers_entreprise`
--
ALTER TABLE `t_pers_entreprise`
  ADD CONSTRAINT `t_pers_entreprise_ibfk_1` FOREIGN KEY (`fk_pers`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_entreprise_ibfk_2` FOREIGN KEY (`fk_entreprise`) REFERENCES `t_entreprise` (`id_Entreprise`);

--
-- Contraintes pour la table `t_pers_mail`
--
ALTER TABLE `t_pers_mail`
  ADD CONSTRAINT `t_pers_mail_ibfk_1` FOREIGN KEY (`fk_pers`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_mail_ibfk_2` FOREIGN KEY (`fk_mail`) REFERENCES `t_mails` (`id_Mail`);

--
-- Contraintes pour la table `t_pers_vehi`
--
ALTER TABLE `t_pers_vehi`
  ADD CONSTRAINT `t_pers_vehi_ibfk_1` FOREIGN KEY (`fk_pers`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_vehi_ibfk_2` FOREIGN KEY (`fk_vehicule`) REFERENCES `t_vehicules` (`id_vehicule`);

--
-- Contraintes pour la table `t_type_vehicule`
--
ALTER TABLE `t_type_vehicule`
  ADD CONSTRAINT `t_type_vehicule_ibfk_1` FOREIGN KEY (`fk_vehicule`) REFERENCES `t_vehicules` (`id_vehicule`);

--
-- Contraintes pour la table `t_vehi_entreprise`
--
ALTER TABLE `t_vehi_entreprise`
  ADD CONSTRAINT `t_vehi_entreprise_ibfk_1` FOREIGN KEY (`fk_entreprise`) REFERENCES `t_entreprise` (`id_Entreprise`),
  ADD CONSTRAINT `t_vehi_entreprise_ibfk_2` FOREIGN KEY (`fk_vehicule`) REFERENCES `t_vehicules` (`id_vehicule`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
